

def sym_vehicle_reader(file_path):
    with open(file_path) as f:
        content = f.readlines()
    content = [x.strip().upper() for x in content]

    sym_dict = {}
    for line in content:
        splits = line.split(',')
        words = [x for x in splits if len(x) > 0]
        for w in words:
            sym_dict[w] = set(words)

    return sym_dict

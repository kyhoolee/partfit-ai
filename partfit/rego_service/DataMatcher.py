import pandas
import ServiceReader
import sys
import math
import numpy
import time

def read_csv(file_path):
    data = pandas.read_csv(file_path)
    # print data
    return data


def read_xlsx(file_path):
    data = pandas.read_excel(file_path, sheetname=0)
    return read_dataframe(data)


def read_dataframe(data):
    result = []
    #data = data.replace(numpy.nan, '', regex=True)

    print(data.columns)
    #print(type(data))

    for index, row in data.iterrows():
        x = {}
        for col in data.columns:
            x[col] = row[col]
        result.append(x)

    #print len(result)
    return result, data


def process_row(columns, row):
    x = {}
    for col in columns:
        x[col] = row[col]
        print col, ' :::: ', row[col]


def compareColumns(query, veh):
    # print len(query), query
    print len(veh), veh

    intersection = query.intersection(veh)
    # print len(intersection), intersection
    # print veh.difference(intersection)
    return intersection


def query_vehicle(columns, query, vehicles):
    select_ = vehicles
    for col in columns:
        if query[col] != None:
            temp_ = select_
            try:
                print col, ' --- ', query[col]
                #if type(vehicles[col]) == type(query[col]):
                temp_ = select_.loc[(vehicles[col] == query[col])]
            except:
                print "Unexpected error:", sys.exc_info()[0]

            if len(temp_) > 0:
                select_ = temp_
            print len(select_)

    print_pandas(select_)


def query_vehicle_rule(bodytype_synonym, columns, query, vehicles):
    select = vehicles

    if query['MakeName'] != None:
        select = filter_vehicle_contain_word(query['MakeName'], 'Make', select)

    if query['Model'] != None:
        select = filter_vehicle_in_word(query['Model'], 'Model', select)

    if query['BodyType'] != None:
        select = filter_bodytype(bodytype_synonym, query['BodyType'], 'BodyType', select)

    if query['cc'] != None:
        select = filter_vehicle_range(query['cc'], 2, 'cc', select)
        # select = filter_vehicle_range_mul(query['cc'], 0.1, 'cc', select)

    if query['KW'] != None:
        select = filter_vehicle_range(query['KW'], 2, 'KW', select)

    if query['ExactYear'] != None:
        select = filter_vehicle_date_range(query['ExactYear'], 1, select)


    if query['FuelType'] != None:
        select = filter_vehicle_contain(query['FuelType'], 'FuelType', select)

    if query['Chassis'] != None:
        select = filter_vehicle_prefix(query['Chassis'], 'Chassis', select)

    return print_array_frame(columns, select)


def filter_vehicle_contain_word(value, column, vehicles):
    temp_ = []
    try:
        print column, ' --- ', value
        for row in vehicles:
            if str(row[column]).find(' ' + str(value)) != -1 \
                    or str(row[column]).find(str(value) + ' ') != -1 \
                    or str(value) == str(row[column]):
                temp_.append(row)
    except:
        print "Unexpected error:", sys.exc_info()[0]

    print len(temp_)
    if len(temp_) > 0:
        return temp_
    else:
        return vehicles


def filter_vehicle_in_word(value, column, vehicles):
    temp_ = []
    try:
        print column, ' --- ', value
        for row in vehicles:
            if str(value).find(' ' + str(row[column])) != -1 \
                    or str(value).find((str(row[column]) + ' ')) != -1 \
                    or str(value) == str(row[column]):
                temp_.append(row)
    except:
        print "Unexpected error:", sys.exc_info()[0]

    print len(temp_)
    if len(temp_) > 0:
        return temp_
    else:
        return vehicles


def filter_vehicle_contain(value, column, vehicles):
    temp_ = []
    try:
        print column, ' --- ', value
        for row in vehicles:
            if str(row[column]).find(str(value)) != -1 or str(value).find(str(row[column])) != -1:
                temp_.append(row)
    except:
        print "Unexpected error:", sys.exc_info()[0]

    print len(temp_)
    if len(temp_) > 0:
        return temp_
    else:
        return vehicles


def filter_vehicle_prefix(value, column, vehicles):
    temp_ = []
    try:
        print column, ' --- ', value
        for row in vehicles:
            if str(row[column]).find(str(value)) == 0 or str(value).find(str(row[column])) == 0:
                temp_.append(row)
    except:
        print "Unexpected error:", sys.exc_info()[0]

    print len(temp_)
    if len(temp_) > 0:
        return temp_
    else:
        return vehicles


def filter_vehicle_date(value, vehicles):
    temp_ = []
    try:
        print 'ExactYear', ' --- ', value
        for row in vehicles:
            if math.isnan(row['YearFrom']) or int(row['YearFrom']) <= value:
                if math.isnan(row['YearTo']) or int(row['YearTo']) >= value:
                    temp_.append(row)
    except:
        print "Unexpected error:", sys.exc_info()[0]
    print len(temp_)


    if len(temp_) > 0:
        return temp_
    else:
        return vehicles


def filter_vehicle_date_range(value, range, vehicles):
    temp_ = []
    try:
        print 'ExactYear', ' --- ', value
        for row in vehicles:
            if math.isnan(row['YearFrom']) or int(row['YearFrom']) <= value + range:
                if math.isnan(row['YearTo']) or int(row['YearTo']) >= value - range:
                    temp_.append(row)
    except:
        print "Unexpected error:", sys.exc_info()[0]
    print len(temp_)


    if len(temp_) > 0:
        return temp_
    else:
        return vehicles


def filter_vehicle_range(value, range, column, vehicles):
    temp_ = []
    try:
        print column, ' --- ', value
        for row in vehicles:
            if row[column] <= (value + range):
                if row[column] >= (value - range):
                    temp_.append(row)
    except:
        print "Unexpected error:", sys.exc_info()[0]
    print len(temp_)
    if len(temp_) > 0:
        return temp_
    else:
        return vehicles


def filter_vehicle_range_mul(value, mul_range, column, vehicles):
    temp_ = []
    try:
        print column, ' --- ', value
        for row in vehicles:
            if row[column] <= value * (1 + mul_range):
                if row[column] >= value * (1 - mul_range):
                    temp_.append(row)
    except:
        print "Unexpected error:", sys.exc_info()[0]
    print len(temp_)
    if len(temp_) > 0:
        return temp_
    else:
        return vehicles


def filter_vehicle(value, column, vehicles):
    temp_ = []
    try:
        print column, ' --- ', value
        for row in vehicles:
            if str(row[column]) == str(value):
                temp_.append(row)
    except:
        print "Unexpected error:", sys.exc_info()[0]
    print len(temp_)
    if len(temp_) > 0:
        return temp_
    else:
        return vehicles


def filter_bodytype(synonym, value, column, vehicles):
    temp_ = []
    try:
        print column, ' --- ', value
        value_set = set()
        if str(value) in synonym:
            value_set = synonym[str(value)]
        value_set.add(str(value))
        print value_set
        for row in vehicles:
            if str(row[column]) in value_set:
                temp_.append(row)
    except:
        print "Unexpected error:", sys.exc_info()[0]
    print len(temp_)
    if len(temp_) > 0:
        return temp_
    else:
        return vehicles


def print_pandas(data_frame):
    for index, row in data_frame.iterrows():
        r = row_to_dict(data_frame.columns, row)
        print r


def print_array_frame(columns, array_frame):
    result = []
    for row in array_frame:
        r = row_to_dict(columns, row)
        result.append(r)
        print r
    return result


def row_to_dict(columns, row):
    r = {}
    for col in columns:
        r[col] = row[col]
    return r


def read_synonym(fname):
    with open(fname) as f:
        content = f.readlines()

    content = [x.strip() for x in content]
    result = {}
    for line in content:
        splits = line.split(',')
        syms = set(splits)
        for w in splits:
            result[w] = syms
    return result


def process(plate_number, vehicles, bodytype_synonym):
    query = ServiceReader.process('rego=' + str(plate_number) + '&CountryCode=NZ&state=NZ')
    query_col = set()
    for q in query:
        for k, v in q.iteritems():
            query_col.add(k)

    veh_col = set(vehicles.columns.values)
    common_col = compareColumns(query_col, veh_col)

    vehicles_array = []
    for index, row in vehicles.iterrows():
        vehicles_array.append(row)

    q_col = set(common_col)

    common_col.add('Make')
    q_col.add('MakeName')

    start_time = time.time()
    result = []
    for q in query:
        v = query_vehicle_rule(bodytype_synonym, common_col, q, vehicles_array)
        qdict = row_to_dict(q_col, q)
        result.append((q_col, qdict, v))

    elapsed_time = time.time() - start_time
    print '--->>> Time: ', elapsed_time
    return result


def process_write_result():
    nz_cars, col = read_xlsx('../../data/NZ_cars-new_map.xlsx')
    nz_cars_short = nz_cars[0:1000]

    f = open('../../data/nz_cars_result_format.txt', 'w')

    bodytype_synonym = read_synonym('../../data/synonym.csv')
    vehicles = read_csv('../../data/Vehicles.csv')
    make_name = read_csv('../../data/Makes.csv')
    vehicles = pandas.merge(vehicles, make_name, left_on='MakeID', right_on='ID', how='inner')
    NZ_vehicles = read_csv('../../data/NewZealand_Vehicles.csv')
    vehicles = pandas.merge(vehicles, NZ_vehicles, on='VehicleID', how='inner')

    count = 0
    t = 0
    for car in nz_cars_short:
        # print car
        count = count + 1
        print '+++++++++++', count, '\n\n'
        try:
            result = process(car['Number plate'], vehicles, bodytype_synonym)

            for (q_col, q,r) in result:
                if len(r) > 0:


                    #f.write(str(car) + '\n')
                    #f.write('-----------------\n')
                    #f.write(str(q) + '\n')
                    #f.write('-----------------\n')
                    for d in r:
                        #x = fill_feature(d, q, q_col)
                        d['Number plate'] = car['Number plate']
                        t = t + 1
                        if t == 1:
                            f.write(key_name(d))
                        f.write(value_name(d) + '\n')
                    #f.write('\n=============\n')
                    #f.write("\n\n")

        except:
            print "Unexpected error:", sys.exc_info()[0]

    f.close()  # you can omit in most cases as the destructor will call it


def key_name(d):
    result = ''
    for k in d:
        result += str(k) + ','
    result = result[:-1]
    return result

def value_name(d):
    result = ''
    for k in d:
        result += str(d[k]) + ','
    result = result[:-1]
    return result


def fill_feature(d, car, q_col):
    r = {}
    for c in q_col:
        r[c] = q_col[c]

    r['YearFrom'] = d['YearFrom']
    r['Capacity'] = d['Capacity']
    r['cc'] = d['cc']
    r['ModelGeneration'] = d['ModelGeneration']
    r['EngineType'] = d['EngineType']
    r['MakeID'] = d['MakeID']
    r['Chassis'] = d['Chassis']
    r['FuelType'] = d['FuelType']
    r['Make'] = d['Make']
    r['Region'] = d['Region']
    r['YearTo'] = d['YearTo']
    r['BrakeSystem'] = d['BrakeSystem']
    r['DriveType'] = d['DriveType']
    r['Model'] = d['Model']
    r['TransmissionType'] = d['TransmissionType']
    r['Submodel'] = d['Submodel']
    r['Valves'] = d['Valves']
    r['VehicleProvider'] = d['VehicleProvider']
    r['BodyType'] = d['BodyType']
    r['VehicleID'] = d['VehicleID']
    r['Series'] = d['Series']
    r['YearRange'] = d['YearRange']
    r['BrakeType'] = d['BrakeType']
    r['FuelMixtureFormation'] = d['FuelMixtureFormation']
    r['KW'] = d['KW']
    r['CatalystConverterType'] = d['CatalystConverterType']

    return r


def sample():
    result = process('ZU8179') # 2 result --> seem fit
    print '##############'
    for (q, r) in result:
        print q
        print '********'
        for d in r:
            print d


if __name__ == '__main__':
    process_write_result()
    #sample()
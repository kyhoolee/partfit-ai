import requests


es_partsfit_vehicle = 'http://anyquiz.info:9200/partsfit/vehicle'


def post(url, content):
    r = requests.post(url, data=content)
    # And done.
    return r.json()


def get(url, params):
    r = requests.get(url, params)
    return r.json()

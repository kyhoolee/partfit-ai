import DataReader
import json
import Utils
import VehicleReader
import SynonymReader
import FieldPairMatcher
import re
import time
import sys
reload(sys)
sys.setdefaultencoding('utf8')


synonym_dict = {}


def build_search(item):
    result = ''
    for key, value in item.iteritems():
        result += str(value) + " "
    return result


def search_es(item):
    data = build_search(item)
    result = Utils.post(Utils.es_partsfit_vehicle, data)
    print result
    return ''


def find_field_set(item, field_set):
    result = set()

    for key, value in item.iteritems():
        val_set = value
        for m in field_set:
            m = str(m)
            if len(m) > 0 and (m in val_set or str(m).replace(' ', '') in val_set):
                result.add(m)

    return result


def find_non_int_field_set(item, field_set):
    result = set()

    for key, value in item.iteritems():
        val_set = value
        for m in field_set:
            m = str(m)
            if not is_int(m):

                if len(m) > 0 and (m in val_set or str(m).replace(' ', '') in val_set):
                    result.add(m)

    return result


def find_field_make_set(item_origin, make_sims_field, field_set):
    result = set()
    item = {}
    for key, value in item_origin.iteritems():
        if key in make_sims_field:
            item[key] = value

    for key, value in item.iteritems():
        val_set = value
        for m in field_set:
            m = str(m)
            if len(m) > 0:
                for val in val_set:
                    m_set = set(re.findall(r"[\w']+|[-.,!?;]", m))
                    if val in m_set and len(val) > 1:
                        result.add(m)
                        break

    return result


# def find_field_set(item, field_set):
#     result = set()
#
#     for key, value in item.iteritems():
#         for m in field_set:
#             val = str(value).encode('utf-8').upper()
#             val_set = set(re.findall(r"[\w']+|[.,!?;]", val))
#             if len(m) > 0:
#                 for val in val_set:
#                     m_set = set(re.findall(r"[\w']+|[-.,!?;]", m))
#                     if val in m_set:
#                         result.add(m)
#                         break
#
#     return result


def find_make(item, make_sims_field, make_set):
    return find_field_make_set(item, make_sims_field, make_set)


def find_model(item_origin, model_sims_field, item_make, make_model_dict, model_set):
    result = set()
    item = {}
    for key, value in item_origin.iteritems() :
        if key in model_sims_field:
            item[key] = value

    if len(item_make) > 0:
        make_model_set = make_model(item_make, make_model_dict)

        result = find_field_set(item, make_model_set)
        if len(result) == 0:
            result = find_non_int_field_set(item, model_set)
        return result
    else:
        result = find_non_int_field_set(item, model_set)

        return result


def find_field(item, item_make, make_field_dict):
    result = set()

    field_set = make_field(item_make, make_field_dict)
    result = find_field_set(item, field_set)

    return result


def find_field_by_sims(item_origin, sims_field, item_model, model_field_dict):
    """
    Find vehicle field values in candidate-fields of item
    :param item_origin: item-data
    :param sims_field: similar field in item
    :param item_model: model of item
    :param model_field_dict: field-value related to model
    :return:
    """
    result = set()

    item = {}
    for key, value in item_origin.iteritems():
        if key in sims_field:
            item[key] = value

    if len(item_model) > 0:
        field_set_by_model = sub_field(item_model, model_field_dict)
        # print field_set_by_model
        result = find_field_set(item, field_set_by_model)
    return result


def find_field_by_sims_field_set(item_origin, sims_field, item_model, model_field_dict, field_set):
    result = set()

    item = {}
    for key, value in item_origin.iteritems():
        if key in sims_field:
            item[key] = value

    if len(item_model) > 0:
        field_set_by_model = sub_field(item_model, model_field_dict)
        result = find_field_set(item, field_set_by_model)
        if len(result) == 0:
            result = find_field_set(item, field_set)
        return result
    else:
        result = find_field_set(item, field_set)

        return result


def make_model(make_set, make_model_dict):
    result = set()

    for make in make_set:
        model_set = make_model_dict[make]
        result |= model_set

    return result


def make_field(make_set, make_field_dict):
    result = set()

    for make in make_set:
        model_set = make_field_dict[make]
        result |= model_set

    return result


def sub_field(origin_set, sub_field_dict):
    """

    :param origin_set:
    :param sub_field_dict:
    :return:
    """
    result = set()

    for origin in origin_set:
        if origin in sub_field_dict:
            sub_set = sub_field_dict[origin]
            result |= sub_set

    return result


def enrich_synonym(value_set):
    result = set()
    result |= value_set
    for v in value_set:
        if v in synonym_dict:
            result |= synonym_dict[v]

    return result


def search_item_es(vehicle_path, item_path, result_file):

    vehicles, col_veh = DataReader.read(vehicle_path)
    make_set = VehicleReader.make_set(vehicles)
    model_set = VehicleReader.field_set(vehicles, 'Model')
    make_model_dict = VehicleReader.make_model_dict(vehicles)

    items, col_item = DataReader.read(item_path)

    veh_field_sims = FieldPairMatcher.vehicle_sim_field(vehicles, col_veh, items, col_item)

    for field in col_veh:
        VehicleReader.init_model_field_dict(vehicles, field)

    count = 0
    for it in items:
        print '\n'
        print 'Item:   ', it
        result_file.write('\n')
        result_file.write('Item:   ' + str(it) + '\n')
        item = {}
        for key, value in it.iteritems():
            val = str(value).encode('utf-8').upper().replace("(", "").replace(")", "")
            val_set = set(re.findall(r"[\w']+|[-.,!?;]", val))

            val_set = enrich_synonym(val_set)

            item[key] = val_set

        read_item_make_model(result_file, vehicles, item, veh_field_sims, make_set, make_model_dict, model_set)
        count += 1
        # if count > 100:
        #     break


def read_item_make_model(result_file, vehicles, item, veh_field_sims, make_set, make_model_dict, model_set):

    make_sims_field = veh_field_sims['Make']
    item_make = find_make(item, make_sims_field, make_set)

    model_sims_field = veh_field_sims['Model']
    item_model = find_model(item, model_sims_field, item_make, make_model_dict, model_set)

    series_sims_field = veh_field_sims['Series']
    series_set = VehicleReader.field_set(vehicles, 'Series')
    make_series_dict = VehicleReader.make_field_dict(vehicles, 'Series')
    item_series = find_field_by_sims(item, series_sims_field, item_make, make_series_dict)

    # CountryName,DateAdded,DateUpdated,Delete,Provider,VehicleID,KtypNr,
    # Make,Model,ModelGeneration,Submodel,Chassis,Series,EngineCode,
    # YearFrom,MonthFrom,YearTo,MonthTo,
    # KW,PS,ccmTech,
    # Lit,Zyl,Ventile,EngineType,FuelMixtureFormation,
    # DriveType,DriveTypeSynonim,
    # BrakeType,BrakeSystem,FuelType,
    # CatalystConverterType,TransmissionType,BodyType

    print 'Make:   ', list(item_make)
    print 'Model:  ', list(item_model)
    print 'Series: ', list(item_series)
    #print ''
    print 'ModelGeneration:   ', list(read_item_field('ModelGeneration', vehicles, veh_field_sims, item, item_model))
    print 'Submodel:          ', list(read_item_field('Submodel', vehicles, veh_field_sims, item, item_model))
    #print ''
    print 'Chassis:       ', list(read_item_field('Chassis', vehicles, veh_field_sims, item, item_model))
    print 'EngineCode:    ', list(read_item_field('EngineCode', vehicles, veh_field_sims, item, item_model))
    print 'EngineType:    ', list(read_item_field('EngineType', vehicles, veh_field_sims, item, item_model))
    print 'BodyType:      ', list(read_item_field('BodyType', vehicles, veh_field_sims, item, item_model))
    print '----------'

    result_file.write('Make:   ' + str(list(item_make)) + '\n')
    result_file.write('Model:  '+ str(list(item_model)) + '\n')
    result_file.write('Series: '+ str(list(item_series)) + '\n')
    #result_file.write('' + '\n')
    result_file.write('ModelGeneration:   '+ str(list(read_item_field('ModelGeneration', vehicles, veh_field_sims, item, item_model))) + '\n')
    result_file.write('Submodel:          '+ str(list(read_item_field('Submodel', vehicles, veh_field_sims, item, item_model))) + '\n')
    #result_file.write('' + '\n' )
    result_file.write('Chassis:       '+ str(list(read_item_field('Chassis', vehicles, veh_field_sims, item, item_model))) + '\n')
    result_file.write('EngineCode:    '+ str(list(read_item_field('EngineCode', vehicles, veh_field_sims, item, item_model))) + '\n')
    result_file.write('EngineType:    '+ str(list(read_item_field('EngineType', vehicles, veh_field_sims, item, item_model))) + '\n')
    result_file.write('BodyType:      '+ str(list(read_item_field('BodyType', vehicles, veh_field_sims, item, item_model))) + '\n')
    #result_file.write('----------\n')
    '''
    # KW,PS,ccmTech,
    # Lit,Zyl,Ventile,FuelMixtureFormation,
    # DriveType,DriveTypeSynonim,
    # BrakeType,BrakeSystem,FuelType,
    # CatalystConverterType,TransmissionType,
    '''
    result_file.write(
        'FuelType:       ' + str(list(read_item_field('FuelType', vehicles, veh_field_sims, item, item_model))) + '\n')
    result_file.write(
        'CatalystConverterType:    ' + str(list(read_item_field('CatalystConverterType', vehicles, veh_field_sims, item, item_model))) + '\n')
    result_file.write(
        'FuelMixtureFormation:    ' + str(list(read_item_field('FuelMixtureFormation', vehicles, veh_field_sims, item, item_model))) + '\n')
    result_file.write(
        'Lit:      ' + str(list(read_item_field('Lit', vehicles, veh_field_sims, item, item_model))) + '\n')

    result_file.write(
        'Zyl:       ' + str(list(read_item_field('Zyl', vehicles, veh_field_sims, item, item_model))) + '\n')
    result_file.write(
        'Ventile:    ' + str(
            list(read_item_field('Ventile', vehicles, veh_field_sims, item, item_model))) + '\n')
    result_file.write(
        'BrakeType:    ' + str(
            list(read_item_field('BrakeType', vehicles, veh_field_sims, item, item_model))) + '\n')
    result_file.write(
        'BrakeSystem:      ' + str(list(read_item_field('BrakeSystem', vehicles, veh_field_sims, item, item_model))) + '\n')

    result_file.write(
        'DriveType:       ' + str(list(read_item_field('DriveType', vehicles, veh_field_sims, item, item_model))) + '\n')
    result_file.write(
        'KW:    ' + str(
            list(read_item_field('KW', vehicles, veh_field_sims, item, item_model))) + '\n')
    result_file.write(
        'PS:    ' + str(
            list(read_item_field('PS', vehicles, veh_field_sims, item, item_model))) + '\n')
    result_file.write(
        'ccmTech:      ' + str(
            list(read_item_field('ccmTech', vehicles, veh_field_sims, item, item_model))) + '\n')
    # print list(item_model_1)


def read_item_field(field_name, vehicles, veh_field_sims, item, item_model):
    """
    Find value of field_name of vehicles in item
    :param field_name: name of the field of vehicle-field-name to find value
    :param vehicles: data of all vehicles
    :param veh_field_sims: fields in the item that similar to vehicle-field-name
    :param item: item data
    :param item_model: the model of item
    :return:
    """
    field_sims_field = veh_field_sims[field_name]
    # field_set = VehicleReader.field_set(vehicles, field_name)
    model_field_dict = VehicleReader.model_field_dict(vehicles, field_name)

    item_field = find_field_by_sims(item, field_sims_field, item_model, model_field_dict)
    return item_field


def is_int(s):
    try:
        int(str(s))
        return True
    except ValueError:
        return False


def read_field(vehicles, columns, item, item_make):

    make_field_dict_map = {}
    for field in columns:
        make_field_dict = VehicleReader.make_field_dict(vehicles, field)
        make_field_dict_map[field] = make_field_dict
    for field in columns:
        print field
        item_field = find_field(item, item_make, make_field_dict_map[field])
        print list(item_field)


def extract_field(result_path, vehicle_path, item_path):
    result_file = open(result_path, 'w')
    search_item_es(vehicle_path, item_path, result_file)

    result_file.close()  # you can omit in most cases as the destructor will call it


if __name__ == '__main__':
    synonym_dict = SynonymReader.sym_vehicle_reader('../data/SynonymVehicle.txt')

    # start_time = time.time()
    # extract_field('../data/log/Milford Industries_extract.txt', '../data/Milford Industries_VML.xlsx',
    #               '../data/Milford Industries_Source.xlsx')
    # print("--- %s seconds ---" % (time.time() - start_time))

    start_time = time.time()
    extract_field('../data/log/Whiteline_VML_extract.txt', '../data/Whiteline_VML.csv',
                  '../data/Whiteline_Source file.xlsx')
    print("--- %s seconds ---" % (time.time() - start_time))
import pandas
import numpy
import os


def read_csv(file_path):
    data = pandas.read_csv(file_path)
    return read_dataframe(data)


def read_xlsx(file_path):
    data = pandas.read_excel(file_path, sheetname=0)
    return read_dataframe(data)


def read_dataframe(data):
    result = []
    data = data.replace(numpy.nan, '', regex=True)

    print(data.columns)
    #print(type(data))

    for index, row in data.iterrows():
        x = {}
        for col in data.columns:
            x[col] = row[col]
        result.append(x)

    #print len(result)
    return result, data


def check_file_type(file_path):
    filename, file_extension = os.path.splitext(file_path)

    return file_extension


def read(file_path):
    file_type = check_file_type(file_path)
    # print(file_type)
    data = []
    if file_type == '.csv':
        #print 'read csv'
        data = read_csv(file_path)
        #print len(data)
    elif file_type == '.xlsx':
        #print 'read xlsx'
        data = read_xlsx(file_path)
        #print len(data)
    return data


if __name__ == '__main__':
    # data = read_csv('../data/Auto Parts Group_VML.csv')
    # print(data)
    # data = read_xlsx('../data/Auto Parts Group_Source File.xlsx')
    # print(data)
    data = read_csv('../data/Vehicles.csv')
    print data

import DataReader
import json
import Utils


def insert_es(vehicle):
    data = json.dumps(vehicle)
    result = Utils.post(Utils.es_partsfit_vehicle, data)
    print result
    return ''


def insert_es_all():

    data = DataReader.read('../data/Auto Parts Group_VML.csv')
    for d in data:
        insert_es(d)


def field_set(data, field):
    result = set()

    for d in data:
        result.add(d[field])
    return result


def clean_make(make):
    country_set = {'USA', 'AUSTRALIA'}
    make = str(make)
    make = make.upper()
    for c in country_set:
        make = make.replace(c, '').strip()

    return make


def clean_field(model):

    model = str(model).strip().upper()

    return model


def make_set(data):
    result = set()

    for d in data:
        result.add(clean_make(d['Make']))

    return result


def make_model_dict(data):
    result = {}

    for d in data:
        make = (clean_make(d['Make']))
        model = (clean_field(d['Model']))
        if make not in result:
            result[make] = set()
        result[make].add(model)

    return result


def model_make_dict(data):
    result = {}

    for d in data:
        make = (clean_make(d['Make']))
        model = (clean_field(d['Model']))
        if make not in result:
            result[model] = set()
        result[model].add(make)

    return result


make_field_dict_cache = {}


def make_field_dict(data, field):
    if field in make_field_dict_cache:
        return make_field_dict_cache[field]
    result = {}

    for d in data:
        make = (clean_make(d['Make']))
        value = (clean_field(d[field]))
        if make not in result:
            result[make] = set()
        if len(value) > 0:
            result[make].add(value)

    make_field_dict_cache[field] = result
    return result


model_field_dict_cache = {}


def init_model_field_dict(data, field):
    result = {}

    for d in data:
        model = (clean_make(d['Model']))
        value = (clean_field(d[field]))
        if model not in result:
            result[model] = set()
        if len(value) > 0:
            splits = value.split(' ')
            for s in splits:
                if len(s) > 0:
                    result[model].add(s)

    model_field_dict_cache[field] = result


def model_field_dict(data, field):
    if field in model_field_dict_cache:
        return model_field_dict_cache[field]

    result = {}

    for d in data:
        model = (clean_make(d['Model']))
        value = (clean_field(d[field]))
        if model not in result:
            result[model] = set()
        if len(value) > 0:
            splits = value.split(' ')
            for s in splits:
                if len(s) > 0:
                    result[model].add(s)

    model_field_dict_cache[field] = result

    return result


if __name__ == '__main__':
    insert_es_all()
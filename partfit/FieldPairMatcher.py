import DataReader
import VehicleReader
import re
import sys
reload(sys)
sys.setdefaultencoding('utf8')


def column_value_set(data, columns):
    result = {}

    for c in columns:
        result[c] = set()
        for d in data:
            val = str(d[c]).encode('utf-8').upper()
            val_set = set(re.findall(r"[\w']+|[.,!?;]", val))
            result[c] |= val_set

    return result


def sim_set(first, second):
    score = 1.0
    score = len(first.intersection(second)) * score / (len(first.union(second)) + 1)
    return score


def read_data(vehicle_path, item_path):
    vehicles, col_veh = DataReader.read(vehicle_path)
    items, col_item = DataReader.read(item_path)

    return vehicles, col_veh, items, col_item


def vehicle_sim_field(vehicles, col_veh, items, col_item):

    ve_col_set = column_value_set(vehicles, col_veh)
    it_col_set = column_value_set(items, col_item)

    sims = vehicle_sim_matrix(ve_col_set, it_col_set, col_veh, col_item)
    ve_rel_field = most_related_field(sims)

    result = {}
    for ve_field in ve_rel_field:
        result[ve_field] = set()
        for (field, score) in ve_rel_field[ve_field]:
            result[ve_field].add(field)

    return result


def vehicle_sim_matrix(ve_col_set, it_col_set, col_veh, col_item):
    result = {}
    for v_c in col_veh:
        result[v_c] = []
        for i_c in col_item:
            result[v_c].append( (i_c, sim_set(ve_col_set[v_c], it_col_set[i_c])) )

        print '\n'

    return result


def item_sim_field(vehicles, col_veh, items, col_item):

    ve_col_set = column_value_set(vehicles, col_veh)
    it_col_set = column_value_set(items, col_item)

    sims = item_sim_matrix(ve_col_set, it_col_set, col_veh, col_item)
    it_rel_field = most_related_field(sims)

    result = {}
    for it_field in it_rel_field:
        result[it_field] = set()
        for (field, score) in it_rel_field[it_field]:
            result[it_field].add(field)

    return result


def item_sim_matrix(ve_col_set, it_col_set, col_veh, col_item):

    result = {}
    for i_c in col_item:
        result[i_c] = []
        for v_c in col_veh:
            result[i_c].append( (v_c, sim_set(ve_col_set[v_c], it_col_set[i_c])) )

        print '\n'

    return result


def most_related_field(sims):
    result = {}

    for field in sims:
        sim_list = sims[field]
        sorted_sim = sorted(sim_list, key=lambda x: x[1], reverse=True)
        filter_sim = [t for t in sorted_sim if t[1] > 0.01]
        if len(filter_sim) == 0:
            filter_sim = [t for t in sorted_sim if t[1] > 0.001]
        filter_sim = filter_sim[0:5]
        print field
        print filter_sim
        print '\n'

        result[field] = filter_sim

    return result


if __name__ == '__main__':
    vehicles, col_veh, items, col_item = read_data('../data/Whiteline_VML.csv', '../data/Whiteline_Source file.xlsx')
        # read_data('../data/Auto Parts Group_VML.csv', '../data/Auto Parts Group_Source File.xlsx')
        #
    result = vehicle_sim_field(vehicles, col_veh, items, col_item)
    for field, sims in result.iteritems():
        print field
        print sims
        print '\n'

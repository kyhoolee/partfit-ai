### Bài toán
* Với input là một query-vehicle với một số thông tin của 1 số các thuộc tính
  - MakeName
  - Model
  - BodyType
  - cc
  - KW
  - ExactYear
  - FuelType
  - Chassis
* Và danh sách 1 số vehicles từ database
* Nhiệm vụ là cần lọc ra các vehicle trong database mà fit với query-vehicle nhất có thể

### Lời giải
* Cách xử lý ở đây là lần lượt dùng từng field thuộc tính của input vehicle để lọc dần các vehicle trong database
* Cụ thể: mô tả trong đoạn code sau
  - Các field được sử dụng theo thứ tự lần lượt do có mức độ ưu tiên field nào nên dùng để lọc trước, field nào nên dùng sau
  - Chi tiết các hàm lọc của từng field sẽ được giải thích sau
```
    // Khởi tạo danh sách các vehicle cần filter chính là tất cả các vechile trong database
    select = vehicles

    // Lọc danh sách vehicle bằng gía trị của field MakeName
    if query['MakeName'] != None:
        // Lọc tất cả các vehicle mà giá trị field Make có chứa giá trị của field MakeName trong query-vehicle
        // Không lọc bằng cách lấy giá trị 2 field bằng nhau vì giá trị của field Make của vehicle trong database thường là tên đầy đủ nên dài hơn
        select = filter_vehicle_contain_word(query['MakeName'], 'Make', select)

    // Lọc danh sách vehicle bằng giá trị của field Model
    if query['Model'] != None:
        // Lọc tất cả các vehicle mà giá trị field Model được chứa trong giá trị của field Model trong query-vehicle
        // Vì giá trị Model của vehicle trong database thường là tên ngắn gọn còn  giá trị Model trong query thường có lại là giá trị model đi kèm với Make
        select = filter_vehicle_in_word(query['Model'], 'Model', select)

    // Lọc danh sách vehicle bằng giá trị của field BodyType
    if query['BodyType'] != None:
        // Chú ý vì BodyType có nhiều giá trị đồng nghĩa nên cần có bodytype_synonym để map từ giá trị của bodytype đến các giá trị đồng nghĩa
        // Hiện tại mới chỉ có 2 cặp từ đồng nghĩa này
        // Saloon,Sedan
        // Utility,Ute
        select = filter_bodytype(bodytype_synonym, query['BodyType'], 'BodyType', select)

    // Lọc danh sách vehicle bằng giá trị của field cc
    if query['cc'] != None:
        // Lọc tất cả các vehicle mà giá trị field cc chênh lệch không quá +-2 so với giá trị của field cc trong query-vehicle
        select = filter_vehicle_range(query['cc'], 2, 'cc', select)

    // Lọc danh sách vehicle bằng giá trị của field KW
    if query['KW'] != None:
        select = filter_vehicle_range(query['KW'], 2, 'KW', select)

    // Lọc danh sách vehicle bằng giá trị của field ExactYear
    if query['ExactYear'] != None:
        // Tương tự field cc thì giá trị field ExactYear cũng dùng để lọc các vehicle mà mà ExeactYear lêch không quá +-1 so với query-vehicle
        select = filter_vehicle_date_range(query['ExactYear'], 1, select)

    // Lọc danh sách vehicle bằng giá trị của field FuelType
    if query['FuelType'] != None:
        // Lọc các vehicle mà giá trị field FuelType chứa trong giá trị field FuelType của query-vehicle
        // Hoặc ngược lại giá trị field FuelType của vehicle chứa giá trị của field FuelType của query-vehicle
        select = filter_vehicle_contain(query['FuelType'], 'FuelType', select)

    // Lọc danh sách vehicle bằng giá trị của field Chassis
    if query['Chassis'] != None:
        // Lọc các vehicle mà giá trị của field Chassis xuất hiện ở đầu trong giá trị của field Chasssis trong query-vehicle
        // Hoặc ngược lại giá trị của field Chassis của cac vehicle chứa giá trị của field Chassis của query-vehicle ở vị trí đầu tiên
        select = filter_vehicle_prefix(query['Chassis'], 'Chassis', select)

    // Trả về danh sách các vehicle đã được filter - với các field cần thiết
    return print_array_frame(columns, select)
```


* Chi tiết các hàm filter được sử dụng
```
def filter_vehicle_contain_word(value, column, vehicles):
    temp_ = []
    try:
        print column, ' --- ', value
        for row in vehicles:
            if str(row[column]).find(' ' + str(value)) != -1 \
                    or str(row[column]).find(str(value) + ' ') != -1 \
                    or str(value) == str(row[column]):
                temp_.append(row)
    except:
        print "Unexpected error:", sys.exc_info()[0]

    print len(temp_)
    if len(temp_) > 0:
        return temp_
    else:
        return vehicles


def filter_vehicle_in_word(value, column, vehicles):
    temp_ = []
    try:
        print column, ' --- ', value
        for row in vehicles:
            if str(value).find(' ' + str(row[column])) != -1 \
                    or str(value).find((str(row[column]) + ' ')) != -1 \
                    or str(value) == str(row[column]):
                temp_.append(row)
    except:
        print "Unexpected error:", sys.exc_info()[0]

    print len(temp_)
    if len(temp_) > 0:
        return temp_
    else:
        return vehicles

def filter_vehicle_contain(value, column, vehicles):
    temp_ = []
    try:
        print column, ' --- ', value
        for row in vehicles:
            if str(row[column]).find(str(value)) != -1 or str(value).find(str(row[column])) != -1:
                temp_.append(row)
    except:
        print "Unexpected error:", sys.exc_info()[0]

    print len(temp_)
    if len(temp_) > 0:
        return temp_
    else:
        return vehicles


def filter_vehicle_date(value, vehicles):
    temp_ = []
    try:
        print 'ExactYear', ' --- ', value
        for row in vehicles:
            if math.isnan(row['YearFrom']) or int(row['YearFrom']) <= value:
                if math.isnan(row['YearTo']) or int(row['YearTo']) >= value:
                    temp_.append(row)
    except:
        print "Unexpected error:", sys.exc_info()[0]
    print len(temp_)


    if len(temp_) > 0:
        return temp_
    else:
        return vehicles


def filter_vehicle_date_range(value, range, vehicles):
    temp_ = []
    try:
        print 'ExactYear', ' --- ', value
        for row in vehicles:
            if math.isnan(row['YearFrom']) or int(row['YearFrom']) <= value + range:
                if math.isnan(row['YearTo']) or int(row['YearTo']) >= value - range:
                    temp_.append(row)
    except:
        print "Unexpected error:", sys.exc_info()[0]
    print len(temp_)


    if len(temp_) > 0:
        return temp_
    else:
        return vehicles


def filter_vehicle_range(value, range, column, vehicles):
    temp_ = []
    try:
        print column, ' --- ', value
        for row in vehicles:
            if row[column] <= (value + range):
                if row[column] >= (value - range):
                    temp_.append(row)
    except:
        print "Unexpected error:", sys.exc_info()[0]
    print len(temp_)
    if len(temp_) > 0:
        return temp_
    else:
        return vehicles


def filter_vehicle_prefix(value, column, vehicles):
    temp_ = []
    try:
        print column, ' --- ', value
        for row in vehicles:
            if str(row[column]).find(str(value)) == 0 or str(value).find(str(row[column])) == 0:
                temp_.append(row)
    except:
        print "Unexpected error:", sys.exc_info()[0]

    print len(temp_)
    if len(temp_) > 0:
        return temp_
    else:
        return vehicles


def filter_vehicle(value, column, vehicles):
    temp_ = []
    try:
        print column, ' --- ', value
        for row in vehicles:
            if str(row[column]) == str(value):
                temp_.append(row)
    except:
        print "Unexpected error:", sys.exc_info()[0]
    print len(temp_)
    if len(temp_) > 0:
        return temp_
    else:
        return vehicles


def filter_bodytype(synonym, value, column, vehicles):
    temp_ = []
    try:
        print column, ' --- ', value
        value_set = set()
        if str(value) in synonym:
            value_set = synonym[str(value)]
        value_set.add(str(value))
        print value_set
        for row in vehicles:
            if str(row[column]) in value_set:
                temp_.append(row)
    except:
        print "Unexpected error:", sys.exc_info()[0]
    print len(temp_)
    if len(temp_) > 0:
        return temp_
    else:
        return vehicles
```

